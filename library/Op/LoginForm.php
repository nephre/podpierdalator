<?php
/**
 * Container for Op_Form class
 *
 * @package     PARIS
 * @subpackage  ${subpackage}
 * @version     $Id$
 * @copyright   2012 OTTO Work Force B.V.
 * @link        https://paris.ottoworkforce.eu
 * @filesource
 */
/**
 * ${description}
 *
 * @package     PARIS
 * @subpackage  ${subpackage}
 * @author      Daniel Jeznach <djeznach@ottopraca.pl>
 */
class Op_LoginForm extends Zend_Form
{
    const E_EMAIL = 'email';
    const E_PASSWORD = 'password';

    public function init()
    {

        $email = new Zend_Form_Element_Text(self::E_EMAIL);
        $email->setLabel('Email');

        $password = new Zend_Form_Element_Password(self::E_PASSWORD);
        $password->setLabel('Hasło');

        $submit = new Zend_Form_Element_Submit('submitter');
        $submit->setLabel('Loguj');

        $this->addElements(array(
            $email,
            $password,
            $submit
        ));

        $this->setMethod('post');
    }

}
