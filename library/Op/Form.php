<?php
/**
 * Container for Op_Form class
 *
 * @package     PARIS
 * @subpackage  ${subpackage}
 * @version     $Id$
 * @copyright   2012 OTTO Work Force B.V.
 * @link        https://paris.ottoworkforce.eu
 * @filesource
 */
/**
 * ${description}
 *
 * @package     PARIS
 * @subpackage  ${subpackage}
 * @author      Daniel Jeznach <djeznach@ottopraca.pl>
 */
class Op_Form extends Zend_Form
{
    const E_USER = 'user';
    const E_TARGET = 'target';
    const E_POINTS = 'points';
    const E_BONUSES = 'bonuses';
    const E_COMMENT = 'comment';

    public function init()
    {
        $users = Doctrine_Query::create()
            ->from('User')
            ->orderBy('name')
            ->execute();
        $data= array('' => '');
        foreach ($users as $user) {
            $data[$user->id_user] = $user->name;
        }


        $user = new Zend_Form_Element_Select(self::E_USER);
        $user->setLabel('Podpierdalający')
            ->setMultiOptions($data)
            ->setRequired()
            ->setValue(Zend_Auth::getInstance()->getIdentity()->id_user);

        $target= new Zend_Form_Element_Select(self::E_TARGET);
        $target->setLabel('Podpierdalany')
            ->setMultiOptions($data)
            ->setRequired();

//        $zaCo = array(
//            ''  => '',
//            '1' => 'Przypadkowe nieumyślne podpierdolenie',
//            '5' => 'Niby nieumyślne podpierdolenie',
//            '10' => 'Zwykła podpierdolka w cztery oczy z przełożonym bez świadków',
//            '20' => 'Anonimowy donos na samego siebie, mimo, iż nie jest się winnym, byle zaistnieć',
//            '30' => 'Podpierdolenie w obecności świadków',
//            '40' => 'Samopodpierdolka w cztery oczy z przełożonym',
//            '50' => 'Samopodpierdolka przy świadkach',
//            '60' => 'Podpierdolenie do przełożonego w obecności podpierdalanego bez innych świadków',
//            '70' => 'Podpierdolenie w obecności podpierdalanego przy większej ilości świadków',
//            '80' => 'Podpierdolenie w obecności podpierdalanego, pomimo, iż ten nie jest winny',
//            '90' => 'Podpierdolenie w obecności podpierdalanego, pomimo, iż ten nie jest winny a czynu tego dokonał sam podpierdalający',
//            '100' => 'Podpierdalanie w obecności podpierdalanego, pomimo, iż ten nie jest winny a czynu tego dokonał sam podpierdalający na spółkę z przełożonym, do którego się podpierdala i wmówienie podpierdalanemu jego winy',
//        );
        $pointsData = Doctrine_Query::create()
            ->from('Point')
            ->orderBy('value ASC')
            ->where('is_bonus = "NO"')
            ->execute();
        $zaCo = array('' => '');
        foreach ($pointsData as $point) {
            $zaCo[$point->id_point] = $point->name;
        }


        $points = new Zend_Form_Element_Select(self::E_POINTS);
        $points->setLabel('Za co?')
            ->setMultiOptions($zaCo)
            ->setRequired();



        $bonusData = Doctrine_Query::create()
            ->from('Point')
            ->orderBy('value ASC')
            ->where('is_bonus = "YES"')
            ->execute();

        $bonusy = array();
        foreach ($bonusData as $point) {
            $bonusy[$point->id_point] = $point->name;
        }

        $bonuses = new Zend_Form_Element_MultiCheckbox(self::E_BONUSES);
        $bonuses->setLabel('Bonusy')
            ->setMultiOptions($bonusy)
            ->setRequired(false);

        $comment = new Zend_Form_Element_Text(self::E_COMMENT);
        $comment->setLabel('Komentarz')
            ->setRequired();

        $submit = new Zend_Form_Element_Submit('submitter');
        $submit->setLabel('Podpierdol');

        $this->addElements(array(
            $user,
            $target,
            $points,
            $bonuses,
            $comment,
            $submit
        ));

        $this->setMethod('post');
    }

}
