<?php
/**
 * Op_RegisterForm class container 
 *
 * @package     R-Infiniti
 * @version     $Id$
 * @copyright   2013 SMT Software S.A.
 * @filesource
 */
/**
 * {descripton}
 *
 * @package     R-Infiniti
 * @author      Daniel Jeznach <daniel.jeznach@smtsoftware.com>
 */
class Op_RegisterForm extends Zend_Form
{
    const E_NAME = 'name';
    const E_EMAIL = 'email';
    const E_PASSWORD = 'password';
    const E_CONFIRMATION = 'confirm';
    const E_CAPTCHA = 'captcha';

    const E_SUBMIT = 'register';

    public function init()
    {
        $name = new Zend_Form_Element_Text(self::E_NAME);
        $name
            ->setRequired()
            ->setLabel('Nazwa')
            ->setDescription('Dobrym pomysłem jest imię i nazwisko');

        $email = new Zend_Form_Element_Text(self::E_EMAIL);
        $email
            ->setRequired()
            ->setLabel('Email')
            ->setDescription('To będzie twój username');

        $password = new Zend_Form_Element_Password(self::E_PASSWORD);
        $password
            ->setLabel('Hasło')
            ->setRequired(true);

        $captcha = new Zend_Form_Element_Captcha(self::E_CAPTCHA, array(
            'label' => "Przepisz tekst z figleta",
            'captcha' => array(
                'captcha' => 'Figlet',
                'wordLen' => 6,
                'timeout' => 300,
            ),
        ));
        $captcha->setDecorators(array(new Zend_Form_Decorator_Captcha(), 'Label', 'Errors'));

//        $imageCaptcha = new Zend_Captcha_Figlet();
//        $imageCaptcha
//            ->setFontSize(12)
//        $captcha
//            ->setCaptcha(new Zend_Captcha_Dumb)
//            ->setLabel('Przepisz to');

        $confirmation = new Zend_Form_Element_Password(self::E_CONFIRMATION);
        $confirmation
            ->setRequired(true)
            ->setLabel('Potwierdzenie')
            ->addValidator(new Zend_Validate_Identical(self::E_PASSWORD));

        $register = new Zend_Form_Element_Submit(self::E_SUBMIT);
        $register->setLabel('Zarejestruj');

        $this->addElements(array(
            $name, $email, $password, $confirmation, $captcha, $register
        ));

        $this->setMethod('post');
    }
}
