<?php
/**
 * DoctrineAdapter class container 
 *
 * @package     R-Infiniti
 * @version     $Id$
 * @copyright   2013 SMT Software S.A.
 * @filesource
 */
/**
 * Doctrine auth adapter
 *
 * @package     R-Infiniti
 * @author      Daniel Jeznach <daniel.jeznach@smtsoftware.com>
 */
class Op_Auth_DoctrineAdapter implements Zend_Auth_Adapter_Interface
{
    /** @var string */
    protected $_username;

    /** @var string */
    protected $_password;

    /**
     * Performs an authentication attempt
     *
     * @throws Zend_Auth_Adapter_Exception If authentication cannot be performed
     * @return Zend_Auth_Result
     */
    public function authenticate()
    {
        $messages = array();
        $identity = null;

        $q = Doctrine_Query::create()
            ->from('User')
            ->where('email = ?', $this->_username)
            ->andWhere('password = ?', md5($this->_username . $this->_password));

        if ($q->count() == 0) {
            $code = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
            $messages[] = 'Ni chu ja';
        } else if ($q->count() > 1) {
            $code = Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS;
            $messages[] = 'Identity ambiguous';
        } else {
            $identity = $q->fetchOne();
            $code = Zend_Auth_Result::SUCCESS;
        }

        return new Zend_Auth_Result($code, $identity, $messages);
    }

    /**
     * Setter for $_username
     *
     * @author Daniel Jeznach <daniel.jeznach@smtsoftware.com>
     * @access public
     *
     * @param  string $username
     * @return Op_Auth_DoctrineAdapter
     */
    public function setUsername($username)
    {
        $this->_username = $username;
        return $this;
    }

    /**
     * Setter for $_password
     *
     * @author Daniel Jeznach <daniel.jeznach@smtsoftware.com>
     * @access public
     *
     * @param  string $password
     * @return Op_Auth_DoctrineAdapter
     */
    public function setPassword($password)
    {
        $this->_password = $password;
        return $this;
    }
}
