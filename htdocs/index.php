<?php

require_once dirname(__FILE__) . '/../application/configs/zend.php';
require_once dirname(__FILE__) . '/../application/configs/doctrine.php';

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();
