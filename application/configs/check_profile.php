<?php
/**
 * Profiles includer
 *
 * @package     R-Infiniti
 * @author      Daniel Jeznach <daniel.jeznach@smtsoftware.com>
 */


$environment = getenv('APPLICATION_ENV');

if (empty($environment)) {
    throw new RuntimeException('No environment variable "APPLICATION_ENV" defined. Unable to continue.');
}

$profilesDir = __DIR__ . DIRECTORY_SEPARATOR . 'profiles';
$profile = $profilesDir . DIRECTORY_SEPARATOR . $environment . '.php';

if (! is_readable($profile)) {
    throw new RuntimeException('Unable to read file with profile settings: ' . $profile);
}

require_once $profile;
