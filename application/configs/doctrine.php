<?php

// this loads profile
require_once __DIR__ . DIRECTORY_SEPARATOR . 'check_profile.php';

//require_once dirname(__FILE__) . '/../../library/Doctrine.php';
require_once 'Doctrine.php';

spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine', 'modelsAutoload'));

$connUrl = 'mysql://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME;

$conn = Doctrine_Manager::connection($connUrl);
$conn->setCharset('utf8');
$conn->setAttribute(Doctrine::ATTR_QUOTE_IDENTIFIER, true);
$conn->execute('SET NAMES "utf8"');
$conn->execute('SET CHARSET "utf8"');

$manager = Doctrine_Manager::getInstance();

$manager->setAttribute(Doctrine::ATTR_AUTO_ACCESSOR_OVERRIDE, true);
$manager->setAttribute(Doctrine::ATTR_AUTOLOAD_TABLE_CLASSES, true);
$manager->setAttribute(Doctrine::ATTR_MODEL_LOADING, Doctrine::MODEL_LOADING_CONSERVATIVE);


/*if (extension_loaded('apc')) {
        $cacheDriver = new Doctrine_Cache_Apc();
} else */
if (extension_loaded('memcache')) {
    $servers = array(
        'host' => '127.0.0.1',
        'port' => 11211,
        'persistent' => true
    );

    $cacheDriver = new Doctrine_Cache_Memcache(array(
        'servers' => $servers,
        'compression' => false
    ));
}

if (isset($cacheDriver)) {
    $manager->setAttribute(Doctrine::ATTR_QUERY_CACHE, $cacheDriver);
    $manager->setAttribute(Doctrine::ATTR_RESULT_CACHE, $cacheDriver);
    $manager->setAttribute(Doctrine::ATTR_RESULT_CACHE_LIFESPAN, 300);
}
