<?php
/**
 * Example profile file
 *
 * @package     R-Infiniti
 * @author      Daniel Jeznach <daniel.jeznach@smtsoftware.com>
 */

// Doctrine
define('DB_USER', 'merlin');
define('DB_PASS', 'merlin');
define('DB_NAME', 'podpierdalator');
define('DB_HOST', '127.0.0.1');
