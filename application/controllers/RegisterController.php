<?php
/**
 * RegisterController class container 
 *
 * @package     R-Infiniti
 * @version     $Id$
 * @copyright   2013 SMT Software S.A.
 * @filesource
 */
/**
 * Register new user
 *
 * @package     R-Infiniti
 * @author      Daniel Jeznach <daniel.jeznach@smtsoftware.com>
 */
class RegisterController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $form = new Op_RegisterForm;

        if ($this->_request->isPost() && $form->isValid($this->_request->getPost())) {
//            $user = new Users
            // do magic
            $user = new User;
            $user->name = $this->_request->{Op_RegisterForm::E_NAME};
            $user->email= $this->_request->{Op_RegisterForm::E_EMAIL};
            $user->password= md5($this->_request->{Op_RegisterForm::E_EMAIL} . $this->_request->{Op_RegisterForm::E_PASSWORD});

            if (! $user->isValid()) {
                $this->view->error = $user->getErrorStackAsString();
            } else {
                $user->save();
                $this->_redirect('/index');
            }
        }

        $this->view->form = $form;
    }
}
