<?php

class IndexController extends Zend_Controller_Action
{
    public function indexAction()
    {

//        $session = new Zend_Session_Namespace('podpierdalator');

        $auth = Zend_Auth::getInstance();

        if(! $auth->hasIdentity()) {
            $this->_redirect('/login');
        }

        $form = new Op_Form;

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();

            if ($form->isValid($data)) {
                $source     = $data[Op_Form::E_USER];
                $target     = $data[Op_Form::E_TARGET];
                $points     = $data[Op_Form::E_POINTS];
                $bonuses    = $data[Op_Form::E_BONUSES];

                if (is_array($bonuses)) {
                    $bonuses = array_values($bonuses);
                } else {
                    $bonuses = array();
                }

                $points = array_merge((array)$points, $bonuses);

                $source = Doctrine::getTable('User')->find($source);
                $target = Doctrine::getTable('User')->find($target);
                $author = Doctrine::getTable('User')->find($auth->getIdentity()->id_user);

                $points = Doctrine_Query::create()
                    ->from('Point')
                    ->whereIn('id_point', $points)
                    ->execute();

                $comment = new Comment;
                $comment->comment = $data[Op_Form::E_COMMENT];
                $comment->save();

                /** @var $point Point */
                foreach ($points as $point) {
                    $delation = new Delation;
                    $delation->DelatingUser = $source;
                    $delation->DelatedUser = $target;
                    $delation->Point = $point;
                    $delation->Author = $author;
                    $delation->Comment = $comment;
                    $delation->save();
                }

                $this->_redirect('/index');
            }
        }

        $this->view->rank = $this->_getRank();
        $this->view->form = $form;
    }

    /**
     * get rank
     *
     * @author Daniel Jeznach <djeznach@ottopraca.pl>
     * @access private
     *
     * @return array
     */
    private function _getRank()
    {
        $dql = Doctrine_Query::create()
            ->select('u.name, u.id_user, IFNULL(SUM(p.value), 0) AS total')
            ->from('User u')
            ->leftJoin('u.Delations d')
            ->leftJoin('d.Point p')
            ->orderBy('total DESC')
            ->groupBy('u.name');
        $result = $dql->execute();
        return $result->toArray();
    }


    public function statsAction()
    {
        // ranking tygodniowy
        // najwięcej podpierdalający
        // TODO: wybierajka tygodni
        $q = Doctrine_Query::create()
            ->select('d.id_delation, u.name, SUM(p.value) AS total, DATE_FORMAT(d.timestamp, "%x:%v") AS yearweek')
            ->from('Delation d')
            ->innerJoin('d.Point p')
            ->innerJoin('d.DelatingUser u')
            ->orderBy('total DESC')
            ->groupBy('yearweek, u.name');

        $this->view->weeklyRank = $q->execute();
    }
}
