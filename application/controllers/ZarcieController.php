<?php
/**
 * Container for ZarcieControler class
 *
 * @package     PARIS
 * @subpackage  ${subpackage}
 * @version     $Id$
 * @copyright   2012 OTTO Work Force B.V.
 * @link        https://paris.ottoworkforce.eu
 * @filesource
 */
/**
 * ${description}
 *
 * @package     PARIS
 * @subpackage  ${subpackage}
 * @author      Daniel Jeznach <djeznach@ottopraca.pl>
 */
class ZarcieController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $places = array(
            'Ikea',
            'Pizza Hut',
            'Kebab',
            'McDonald\'s',
            'Auchan',
            'Tesco/waga',
            'KFC',
        );

        $today = Zend_Date::now();
        $today->setHour(11)->setMinute(30)->setSecond(0);
        $today->addMinute(mt_rand(-30, 30));

        $transport = array(
            'Daniel',
            'Tomek',
            'Z buta',
        );

        $payment = array(
            'Marcin',
            'Tomek',
        );

        $this->view->place= $this->_randVal($places);
        $this->view->when = $today->get('H:m');
        $this->view->transport = $this->_randVal($transport);
        $this->view->payment = $this->_randVal($payment);
    }

    /**
     * Rand arr val
     *
     * @author Daniel Jeznach <djeznach@ottopraca.pl>
     * @access private
     *
     * @param  array $data
     * @return string
     */
    private function _randVal(array $data)
    {
        return $data[mt_rand(0, count($data) - 1)];
    }

}
