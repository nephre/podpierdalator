<?php

class LoginController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();

        if ($auth->hasIdentity()) {
            $this->_redirect('/index');
        }

//        $session = new Zend_Session_Namespace('podpierdalator');
        $form = new Op_LoginForm;

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();

            if ($form->isValid($data)) {
                $email = $data[Op_LoginForm::E_EMAIL];
                $password = $data[Op_LoginForm::E_PASSWORD];

                $authAdapter = new Op_Auth_DoctrineAdapter;
                $authAdapter
                    ->setUsername($email)
                    ->setPassword($password);

                $result = $authAdapter->authenticate();

                if ($result->isValid()) {
//                    $session->id_user = $result->getIdentity()->id_user;
                    $auth->getStorage()->write($result->getIdentity());
                    $this->_redirect('/index');
                } else {
                    $this->view->error = "Invalid username / password";
                }
            }
        }

        $this->view->form = $form;
    }
}
