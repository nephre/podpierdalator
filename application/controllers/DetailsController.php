<?php

class DetailsController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();

        if (! $auth->hasIdentity()) {
            $this->_redirect('/index');
        }
//        $session = new Zend_Session_Namespace('podpierdalator');
        $user_id = $this->_getParam('user_id');

        $user = Doctrine_Query::create()
            ->from('User u')
            ->where('u.id_user = ?', $user_id)
            ->execute();


        $this->view->user_name = $user[0]['name'];

        $delations = Doctrine_Query::create()
            ->from('Delation d')
            ->leftJoin('d.Comment')
            ->leftJoin('d.Author')
            ->leftJoin('d.DelatedUser')
            ->leftJoin('d.Point')
            ->where('d.id_source_user = ?', $user_id)
            ->execute();
        /** @var $delation Delation
        foreach ($delations as $delation)
            echo $delation->Comment;*/
        $this->view->delations = $delations;
    }


}
