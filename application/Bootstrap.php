<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function __construct($application) {
        parent::__construct($application);

        $application->setAutoloaderNamespaces(array('Doctrine', 'Op'));
        Doctrine::loadModels(APPLICATION_PATH . '/models');

        Zend_Session::start();
    }

    protected function _initDoctype()
    {
//        $this->bootstrap('view');
        $view = new Zend_View;
//        $view = $this->getResource('view');
        // HTML 4 Transitional
        $view->doctype(Zend_View_Helper_Doctype::HTML4_LOOSE);
        // Encoding
        $view->headMeta()->appendHttpEquiv('Content-type', 'text/html; charset=utf-8');
        $view->headTitle('Podpierdalator 2 - Reloaded');
        $view->headLink(array('href'=>'/css/style.css', 'rel'=>'stylesheet'));

    }

    protected function _initControllers()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->setControllerDirectory(realpath(__DIR__ . '/controllers'));
    }
}

