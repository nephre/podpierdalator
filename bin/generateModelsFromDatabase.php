<?php

//echo 'Models already generated, terminating...';
//die();

require_once dirname(__FILE__) . '/../application/configs/zend.php';
require_once dirname(__FILE__) . '/../application/configs/doctrine.php';

$options = array('packagesPrefix' =>  'Package',
             'packagesPath'          =>  '',
             'packagesFolderName'    =>  'packages',
             'suffix'                =>  '.php',
             'generateTableClasses'  =>  true,
             'generateBaseClasses'   =>  true,
             'baseClassesPrefix'     =>  'Abstract',
             'baseClassesDirectory'  =>  'abstract',
             'baseClassName'         =>  'Doctrine_Record');

$connection = Doctrine_Manager::connection();
Doctrine::generateModelsFromDb(APPLICATION_PATH . '/models', array($connection->getName()), $options);
