CREATE TABLE IF NOT EXISTS `users` (
    `id_user` int unsigned not null auto_increment,
    `name` varchar(255) not null,
    `scores` smallint unsigned not null default 0,
    `email` varchar(255) not null,
    `password` varchar(255) not null,
    PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET utf8;

CREATE TABLE `points` (
    `id_point` int unsigned not null AUTO_INCREMENT,
    `name` varchar(255) not null,
    `value` smallint unsigned not null,
    `is_bonus` enum('YES', 'NO') not null default 'NO',
    PRIMARY KEY (`id_point`)
) ENGINE = InnoDB DEFAULT CHARSET utf8;

CREATE TABLE `comments` (
  `id_comment` int unsigned not null AUTO_INCREMENT,
  `comment` varchar(255) not null,
  PRIMARY KEY (`id_comment`)
) ENGINE = InnoDB DEFAULT CHARSET utf8;

CREATE TABLE `delations` (
    `id_delation` int unsigned  not null AUTO_INCREMENT,
    `id_source_user` int unsigned not null,
    `id_target_user` int unsigned not null,
    `id_point` int unsigned not null,
    `id_author` int unsigned not null,
    `id_comment` int unsigned not null,
    `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id_delation`),
    CONSTRAINT `fk_source` FOREIGN KEY (`id_source_user`)
    REFERENCES `users` (`id_user`),
    CONSTRAINT `fk_target` FOREIGN KEY (`id_target_user`)
    REFERENCES `users` (`id_user`),
    CONSTRAINT `fk_author` FOREIGN KEY (`id_author`)
    REFERENCES `users` (`id_user`),
    CONSTRAINT `fk_comment` FOREIGN KEY (`id_comment`)
    REFERENCES `comments` (`id_comment`),
    CONSTRAINT `fk_point` FOREIGN KEY (`id_point`)
    REFERENCES `points` (`id_point`)
) ENGINE = InnoDB DEFAULT CHARSET utf8;

