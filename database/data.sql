START TRANSACTION;

INSERT INTO `users` (`name`, `email`, `password`)
VALUES
('Lukasz Koszela', 'lukasz.koszela@smtsoftware.com', '03c7af1369706c627d8e49a004c6597b'),
('Lukasz Mordawski', 'lukasz.mordawski@smtsoftware.com', 'c92fc8bf1eef497f5fd9723acaf1ca1a'),
('Andrzej Figuła', 'andrzej.figula@smtsoftware.com', '03c7af1369706c627d8e49a004c6597b'),
('Daniel Jeznach', 'daniel.jeznach@smtsoftware.com', '03c7af1369706c627d8e49a004c6597b'),
('Piotr Kasperski', 'piotr.kasperski@smtsoftware.com', 'e90c39866cffa79588570a6de1fda017'),
('Sobiesław Hrehorowicz', 'sobieslaw.hrehorowicz@smtsoftware.com', '03c7af1369706c627d8e49a004c6597b'),
('Maciej Kosiński', 'maciej.kosinski@smtsoftware.com', '03c7af1369706c627d8e49a004c6597b'),
('Paweł Morawski', 'pawel.morawski@smtsoftware.com', '03c7af1369706c627d8e49a004c6597b'),
('Oskar Lakner', 'oskar.lakner@smtsoftware.com', '03c7af1369706c627d8e49a004c6597b'),
('Daniel Zaborowski', 'daniel.zaborowski@smtsoftware.com', '03c7af1369706c627d8e49a004c6597b'),
('Lukasz Krzyszczak', 'lukasz.krzyszczak@smtsoftware.com', '0a0630473e9fd3cf91ef59dfa03abef1'),
('Krzysztof Malisiewicz', 'krzysztof.malisiewicz@smtsoftware.com', '28fae6be5ac14009b09f115203487939');

INSERT INTO `points` (`name`, `value`, `is_bonus`)
VALUES
('Przypadkowe nieumyślne podpierdolenie', 1, 'NO'),
('Niby nieumyślne podpierdolenie', 5, 'NO'),
('Zwykła podpierdolka w cztery oczy z przełożonym bez świadków', 10, 'NO'),
('Anonimowy donos na samego siebie, mimo, iż nie jest się winnym, byle zaistnieć', 20, 'NO'),
('Podpierdolenie w obecności świadków', 30, 'NO'),
('Samopodpierdolka w cztery oczy z przełożonym', 40, 'NO'),
('Samopodpierdolka przy świadkach', 50, 'NO'),
('Podpierdolenie do przełożonego w obecności podpierdalanego bez innych świadków', 60, 'NO'),
('Podpierdolenie w obecności podpierdalanego przy większej ilości świadków', 70, 'NO'),
('Podpierdolenie w obecności podpierdalanego, pomimo, iż ten nie jest winny', 80, 'NO'),
('Podpierdolenie w obecności podpierdalanego, pomimo, iż ten nie jest winny a czynu tego dokonał sam podpierdalający', 90, 'NO'),
('Podpierdalanie w obecności podpierdalanego, pomimo, iż ten nie jest winny a czynu tego dokonał sam podpierdalający na spółkę z przełożonym, do którego się podpierdala i wmówienie podpierdalanemu jego winy', 100, 'NO'),
('podpierdolenie najlepszego kumpla', 10, 'YES'),
('podpierdolenie zanim czyn zostanie dokonany', 20, 'YES'),
('kiedy dzień wcześniej piło się z podpierdalanym jego wódkę, w jego domu', 25, 'YES');

COMMIT;
